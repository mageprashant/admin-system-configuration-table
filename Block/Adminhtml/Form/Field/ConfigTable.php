<?php
namespace MagePrashant\ConfigTable\Block\Adminhtml\Form\Field;

use \Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use \Magento\Framework\DataObject;

class ConfigTable extends AbstractFieldArray
{
    protected $optionField;

    protected function _prepareToRender()
    {
		$this->addColumn('slice_label', ['label' => __('Slice Label'), 'class' => 'required-entry']);
        $this->addColumn('coupon_type', [
            'label' => __('Coupon Type'),
            'renderer' => $this->getOptionField(),
        ]);
        $this->addColumn('coupon_value', ['label' => __('Coupon Value'), 'class' => 'required-entry']);
		$this->addColumn('gravity', ['label' => __('Gravity (%)'), 'class' => 'required-entry']);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add New Row');
    }

    /**
     * @return \SR\MagentoCommunity\Block\Adminhtml\Form\Field\OptionField
     */
    protected function getOptionField()
    {
        if (!$this->optionField) {
            $this->optionField = $this->getLayout()->createBlock(
                OptionField::class,
                '',
                ['data' => ['is_render_to_js_template' => true]]
            );
        }

        return $this->optionField;
    }

    /**
     * Prepare existing row data object
     *
     * @param DataObject $row
     * @return void
     */
    protected function _prepareArrayRow(DataObject $row)
    {
        $option = $row->getCouponType();
        $options = [];
        if ($option) {
            $options['option_' . $this->getOptionField()->calcOptionHash($option)]
                = 'selected="selected"';
        }
        $row->setData('option_extra_attrs', $options);
    }
}