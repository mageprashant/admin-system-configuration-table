<?php
namespace MagePrashant\ConfigTable\Block\Adminhtml\Form\Field;

use Magento\Framework\View\Element\Html\Select;

class OptionField extends Select
{
    /**
     * {@inheritdoc}
     */
    protected function _toHtml()
    {
        if (!$this->getOptions()) {

            $options = [
                [
                    'value' => 1,
                    'label' => 'Option 1'
                ],
                [
                    'value' => 2,
                    'label' => 'Option 2'
                ],
                [
                    'value' => 3,
                    'label' => 'Option 3'
                ]
            ];
            $this->setOptions($options);
        }

        return parent::_toHtml();
    }

    /**
     * Sets name for input element
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }
}